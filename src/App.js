import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Layout from './UI/Layout/Layout';
import News from "./container/News/News";
import SendForm from './container/SendFormNews/SendForm';
import OneNews from "./container/oneNews/OneNews";

class App extends Component {
  render() {
    return (
      <div className="container">
          <Layout>
              <Switch>
                  <Route path="/" exact component={News}/>
                  <Route path="/addnews" exact component={SendForm}/>
                  <Route path="/onenews/:id" component={OneNews}/>
              </Switch>
          </Layout>
      </div>
    );
  }
}

export default App;
