import React from 'react';
import {Button, ControlLabel, FormControl, FormGroup, HelpBlock} from "react-bootstrap";

import './SendForm.css';
import {addNews} from "../../store/actions";
import {connect} from "react-redux";


function FieldGroup({ id, label, help, ...props }) {
    return (
        <FormGroup controlId={id}>
            <ControlLabel>{label}</ControlLabel>
            <FormControl {...props} />
            {help && <HelpBlock>{help}</HelpBlock>}
        </FormGroup>
    );
}

class SendForm extends React.Component {

    state = {
        header: '',
        body: '',
        image: ''
    };

    inputChangeHandler = e => {
        this.setState({[e.target.name] : e.target.value})
    };

    fileChangeHandler = e => {
        this.setState({[e.target.name] : e.target.files[0]})
    };

    sendMessageOnServer = (e) => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
           formData.append(key, this.state[key]);
        });
        this.props.sendMessage(formData).then(
            () => {
                this.props.history.push('/');
            }
        );
    };

    render() {

        return (
            <div>
                <form onSubmit={this.sendMessageOnServer}>
                    <FieldGroup
                        id="formControlsText"
                        type="text"
                        name="header"
                        label="Author"
                        placeholder="Enter author name"
                        value={this.state.header}
                        onChange={this.inputChangeHandler}
                    />

                    <FormGroup controlId="formControlsTextarea">
                        <ControlLabel>Message</ControlLabel>
                        <FormControl
                            required
                            name="body"
                            componentClass="textarea"
                            placeholder="Enter Your message"
                            value={this.state.body}
                            onChange={this.inputChangeHandler}
                        />
                    </FormGroup>

                    <FieldGroup
                        id="formControlsFile"
                        type="file"
                        label="File"
                        name="image"
                        help="Click from add file"
                        onChange={this.fileChangeHandler}
                    />

                    <Button type="submit">Submit</Button>
                </form>
            </div>
        );
    }
}

const mapDispachToProps = dispatch => {
    return {
        sendMessage: (data) => dispatch(addNews(data)),
    }
};

export default connect(null, mapDispachToProps)(SendForm);