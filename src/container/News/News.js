import React, {Component} from 'react';
import './News.css';
import { connect } from 'react-redux';
import {fetchMessages, fetchDeleteNews} from "../../store/actions";
import {Button, Image} from "react-bootstrap";


class Desk extends Component {


    componentDidMount() {
        this.props.fetchMessages();
    }

    getOnThisPost (id){
        this.props.history.push(`/onenews/${id}`)
    }

    render() {

        const deleteMessage = (id) => {
            this.props.fetchDeleteNews(id).then(
                () => this.props.fetchMessages()
            );
        };

        const URLImage = 'http://localhost:8000/uploads/';

        return (
            <div id='containerMessage' className="container">
                {this.props.messages.map((message, index) => {
                    return (
                        <div className="message" key={index}>
                            <p  className="time">{message.date.slice(11, 20)}</p>
                            <p onClick={() => this.getOnThisPost(message.id)} className="author">{message.header}</p>
                            <p className="said">{message.body}</p>
                            {message.image && <Image style={{width: '100px'}} src={URLImage+message.image}/>}
                            <Button onClick={() => deleteMessage(message.id)} className='button'>delete</Button>
                        </div>
                    )
                })}
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        messages: state.messages,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchMessages: () => dispatch(fetchMessages()),
        fetchDeleteNews: (id) => dispatch(fetchDeleteNews(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Desk);