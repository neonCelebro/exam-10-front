import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import {fetchComments, fetchDeleteComment, fetchOneNews} from "../../store/actions";
import {Button, Image} from "react-bootstrap";
import SendFormComment from "../SendFormComment/SendFormComment";


class OneNews extends Component {


    componentDidMount() {
        this.props.fetchOneNews(this.props.match.params.id).then(
            this.props.fetchComments(this.props.match.params.id)
        );
    }
    render() {

        const deleteComment = (id) => {
            this.props.deleteComment(id).then(
                () => this.props.fetchComments(this.props.match.params.id)
            );
        };

        const URLImage = 'http://localhost:8000/uploads/';

        return (
            <Fragment>
            <div id='containerMessage' className="container">
                        <div className="message">
                            {Object.keys(this.props.news).length > 1 ?
                                <div>
                                <p className="time">{this.props.news.date.slice(11, 20)}</p>
                                <p className="author">{this.props.news.header}</p>
                                <p className="said">{this.props.news.body}</p>
                                {this.props.news.image && <Image style={{width: '100px'}} src={URLImage+this.props.news.image}/>}
                                </div>
                            : <p>ща...ща...ща...</p>}
                            {this.props.comments.length > 0?
                                this.props.comments.map((key) => {
                                    return <div className="message">
                                        <p className="author">{key.author}</p>
                                        <p className="said">{key.comment}</p>
                                        <Button onClick={() => deleteComment(key.id)} className='button'>delete</Button>
                                    </div>
                                }) : null
                            }
                        </div>
            </div>
            <SendFormComment paramID={this.props.match.params.id}/>
            </Fragment>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        news: state.news,
        comments: state.comments
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchComments: id => dispatch(fetchComments(id)),
        fetchOneNews: id => dispatch(fetchOneNews(id)),
        deleteComment : id => dispatch(fetchDeleteComment(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(OneNews);