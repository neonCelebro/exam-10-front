import React from 'react';
import {Button, ControlLabel, FormControl, FormGroup, HelpBlock} from "react-bootstrap";

import './SendFormComment.css';
import {addComment, fetchComments} from "../../store/actions";
import {connect} from "react-redux";


function FieldGroup({ id, label, help, ...props }) {
    return (
        <FormGroup controlId={id}>
            <ControlLabel>{label}</ControlLabel>
            <FormControl {...props} />
            {help && <HelpBlock>{help}</HelpBlock>}
        </FormGroup>
    );
}

class SendComment extends React.Component {

    state = {
        author: 'ananimus',
        comment: '',
        idNews: this.props.paramID
    };

    inputChangeHandler = e => {
        this.setState({[e.target.name] : e.target.value})
    };

    sendMessageOnServer = (e) => {
        e.preventDefault();
        this.props.sendComment(this.state).then(
            this.props.fetchComments(this.props.paramID)
        );
    };

    render() {

        return (
            <div>
                <form onSubmit={this.sendMessageOnServer}>
                    <FieldGroup
                        id="formControlsText"
                        type="text"
                        name="author"
                        label="Author"
                        placeholder="Enter author name"
                        value={this.state.author}
                        onChange={this.inputChangeHandler}
                    />

                    <FormGroup controlId="formControlsTextarea">
                        <ControlLabel>Message</ControlLabel>
                        <FormControl
                            required
                            name="comment"
                            componentClass="textarea"
                            placeholder="Enter Your message"
                            value={this.state.comment}
                            onChange={this.inputChangeHandler}
                        />
                    </FormGroup>

                    <Button type="submit">Submit</Button>
                </form>
            </div>
        );
    }
}

const mapDispachToProps = dispatch => {
    return {
        sendComment: (data) => dispatch(addComment(data)),
        fetchComments: id => dispatch(fetchComments(id)),
    }
};

export default connect(null, mapDispachToProps)(SendComment);