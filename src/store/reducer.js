import * as actions from './actions';

const initialState = {
    messages: [],
    news: {},
    comments: [],
    isLoaded: false,
    error: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
      case actions.FETCH_REQUEST:
        return {...state, isLoaded: true};
      case actions.FETCH_SUCCSESS:
        return {...state, messages: action.data};
      case actions.FETCH_ONE_NEWS_SUCCSESS:
          return {...state, news: action.data};
      case actions.FETCH_ERROR:
          return {...state, error: action.data};
      case actions.FETCH_COMMENTS:
          return {...state, comments : action.data};
      case actions.FETCH_NEW_MESSAGE:
              return {...state, messages: action.data};
      default: return state;
  }
};

export default reducer;