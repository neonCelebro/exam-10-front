import axios from '../axios';

export const FETCH_REQUEST = 'FETCH_REQUEST';
export const FETCH_SUCCSESS = 'FETCH_SUCCSESS';
export const FETCH_ONE_NEWS_SUCCSESS = 'FETCH_ONE_NEWS_SUCCSESS';
export const FETCH_COMMENTS = 'FETCH_COMMENTS';
export const FETCH_ERROR = 'FETCH_ERROR';
export const FETCH_NEW_MESSAGE = 'FETCH_NEW_MESSAGE';

export const fetchRequest = () => {
    return {type: FETCH_REQUEST}
};

export const fetchSuccsess = data => {
    return {type: FETCH_SUCCSESS, data}
};

export const fetchOneNewsSuccsess = data => {
    return {type: FETCH_ONE_NEWS_SUCCSESS, data}
};

export const fetchOneNewsComments = data => {
    return {type: FETCH_COMMENTS, data}
};

export const fetchError = error => {
    return {type: FETCH_ERROR, error}
};



export const addNews = data => {
    return (dispatch) => {
        dispatch(fetchRequest());
        return axios.post('/news', data).then(
            error => dispatch(fetchError(error.data)),
        )
    }
};

export const addComment = data => {
    return (dispatch) => {
        dispatch(fetchRequest());
        return axios.post('/comments', data).then(
            error => dispatch(fetchError(error.data)),
        )
    }
};

export const fetchMessages = () => {
    return (dispatch) => {
        return axios.get('/news')
            .then(response => dispatch(fetchSuccsess(response.data)))
    }
};

export const fetchOneNews = (id) => {
    return (dispatch) => {
        return axios.get(`/news/${id}`)
            .then(response => dispatch(fetchOneNewsSuccsess(response.data)))
    }
};

export const fetchComments = (id) => {
    return (dispatch) => {
        return axios.get(`/comments?news_id=${id}`)
            .then(response => dispatch(fetchOneNewsComments(response.data)))
    }
};

export const fetchDeleteNews = (id) => {
    return (dispatch) => {
        return axios.delete(`/news/${id}`)
            .then(
                error => dispatch(fetchError(error.data)));
    }
};

export const fetchDeleteComment = (id) => {
    return (dispatch) => {
        return axios.delete(`/comments/${id}`)
            .then(
                error => dispatch(fetchError(error.data)));
    }
};