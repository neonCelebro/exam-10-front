import React from 'react';
import {NavLink} from 'react-router-dom';
import './Header.css';

const Header = () => {
    return (
        <header className="Toolbar">
            <nav>
                <NavLink className="Link" to="/">News</NavLink>
                <NavLink className="Link" to="/addnews">add News</NavLink>
            </nav>
        </header>
    )
};

export default Header;